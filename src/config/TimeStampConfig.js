module.exports = {

    // Request identifier
    NONCE: 1,

    // Identifier of the timestamp within a batch
    NONCE_OF_TIMESTAMP: 1,

    // Available values: "SHA1", "SHA256" e "SHA512".
    HASH_ALGORITHM: 'SHA256',

    // format type
    FORMAT_HASH: 'HASH',

    // format type
    FORMAT_FILE: 'FILE',

    // location where the document is stored
    DOCUMENT_PATH: './documento/image.png',

    // hash of document
    DOCUMENT_HASH: 'FC30043F87BC8A9B9DEB9F663882C4652A34AD8E53D2A74317F9CD4D8CFB33D0'
}