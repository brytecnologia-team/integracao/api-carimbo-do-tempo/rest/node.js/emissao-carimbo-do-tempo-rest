const request = require('request');
const serviceConfig = require('../config/ServiceConfig');
const timeStampConfig = require('../config/TimeStampConfig');

var fs = require('fs');

const URL_TIMESTAMP_SERVER = serviceConfig.URL_TIMESTAMP_SERVER;

this.headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${serviceConfig.ACCESS_TOKEN}`
};

var auxAuthorization = this.headers.Authorization.split(' ');

const configuredToken = () => {
    if (auxAuthorization[1] === '<INSERT_VALID_ACCESS_TOKEN>') {
        console.log('Set up a valid token');
        return false;
    }
    return true;
}


const toStampContent = (URL_TIMESTAMP_SERVER) => {

    const timeStampForm = {
        'nonce': timeStampConfig.NONCE,
        'hashAlgorithm': timeStampConfig.HASH_ALGORITHM,
        'format': timeStampConfig.FORMAT_FILE,
        'documents[0][nonce]': timeStampConfig.NONCE_OF_TIMESTAMP,
        'documents[0][content]': fs.createReadStream(timeStampConfig.DOCUMENT_PATH)
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_TIMESTAMP_SERVER, formData: timeStampForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

const toStampDocumentHash = (URL_TIMESTAMP_SERVER) => {

    const timeStampForm = {
        'nonce': timeStampConfig.NONCE,
        'hashAlgorithm': timeStampConfig.HASH_ALGORITHM,
        'format': timeStampConfig.FORMAT_HASH,
        'documents[0][nonce]': timeStampConfig.NONCE_OF_TIMESTAMP,
        'documents[0][content]': timeStampConfig.DOCUMENT_HASH
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_TIMESTAMP_SERVER, formData: timeStampForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

module.exports = { URL_TIMESTAMP_SERVER, toStampContent, toStampDocumentHash, configuredToken, sleep };