const { URL_TIMESTAMP_SERVER, toStampContent, toStampDocumentHash, configuredToken, sleep } = require('../functions/Stamp');


function timeStampReport(result) {
    var numberOfTimeStampsDone = result.timeStamps.length;

    console.log();

    console.log('Amount timestamps realized: ', numberOfTimeStampsDone);

    console.log();

    console.log('Nonce of timestamp: ', result.timeStamps[0].nonce);

    console.log();

    console.log('Encoded timestamp in base64: ', result.timeStamps[0].content);

}

async function main() {
    if (configuredToken()) {

        console.log('================Starting timestamp by document... ================');

        toStampContent(URL_TIMESTAMP_SERVER).then(result => {
            //console.log('JSON Response of Timestamp by document: ', result);
            timeStampReport(result);

        }).catch(error => {
            console.log(error);
        })

        await sleep(4000);
        console.log();
        console.log('============Starting timestamp by document hash ... ============');

        toStampDocumentHash(URL_TIMESTAMP_SERVER).then(result => {
            //console.log('JSON Response of Timestamp by document hash: ', result);

            timeStampReport(result);


        }).catch(error => {
            console.log(error);
        })

    }
}

main();